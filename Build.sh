#!/bin/sh
# The EPICS commands are deactivated by default

# 1. Build Podman Images
podman build -t=muppet_image --file ~/MUPPET/Podman/MUPPET/Dockerfile && \
#podman build -t=epics_image --file ~/MUPPET/Podman/EPICS/Dockerfile && \

# 2. Run Podman Images
podman run --name=MUPPET -dit --security-opt label=disable --device /dev/spidev1.0:/dev/spidev1.0 --annotation run.oci.keep_original_groups=1 --tz=local -v ~/MUPPET:/home/MUPPET -e PPG_STAT_INT=0.1 -p 62347-62348:62347-62348 -p 127.0.0.1:62350:62350 -p 8888:8888 -e DEV_PREFIX=$HOSTNAME muppet_image || true && \
#podman run --name=EPICS -dit --network="host" --tz=local -e IP_MUPPET=127.0.0.1 -e DEV_PREFIX=$HOSTNAME epics_image || true && \

# 3. Create systemd services for autostart
mkdir -p ~/.config/systemd/user && \
podman generate systemd --name MUPPET > ~/.config/systemd/user/container-MUPPET.service && \
#podman generate systemd --name EPICS > ~/.config/systemd/user/container-EPICS.service && \

systemctl --user daemon-reload && \
systemctl --user enable container-MUPPET.service # && \
#systemctl --user enable container-EPICS.service

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is a small program for the automatic control of the MUPPET board to use
it as an PPG and MCA / TDC with one input channel. The program gets commands via TCP and answers to a
connected host PC. The communication via TCP/IP is done with a bytestream.

There is also an interface implemented to transfer debug information via zeromq
to another script, a jupyter notebook (via REQ/REP), and a connection rdy to be used by EPICS
StreamDevice. All these communication is done via strings.

The IP address of the host as well as the port number have to be adjusted!  # TODO Text nochmal updaten

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Dennis Neidherr, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: D.Neidherr@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""

import sys
import os
# import numpy as np
import socket
import time
# import struct
import threading
import queue
import zmq
from typing import List
sys.path.insert(0, '/home/MUPPET/Python/pyfcmf')  # Sequencer Module path in container
sys.path.insert(0, '/home/pi/MUPPET/Python/pyfcmf')  # Sequencer Module path on host
import sequencer as ms


# CONSTANTS / PARAMETERS
HOST = '0.0.0.0'  # Set RasPI IP Address here; '0.0.0.0' is everything - containers need something like 10.0.2.100 (gateway), outside IPs may change
EPICS_ACTIVE = False  # Acivate or deactivate the EPICS server
PPG_CMD_PORT = 62347  # Set Port for the PPG_CMD TCP-Server here
PPG_STATUS_PORT = 62348  # Set Port for the PPG_STATUS TCP-Server here
EPICS_PORT = 62350  # Set Port for the communication with the EPICS IOC
PPG_STATUS_READOUT_INTERVAL = float(os.environ.get('PPG_STAT_INT', '0.1'))  # The interval in which the status of the PPG is published, ENV value is taken, Default value is 0.1
TCP_TERM_CHAR = '\n'  # Termination character for the TCP communication
DEBUG_LVL = 1  # Debug lvl for writing to the MUPPET_Backend.log file: 0 - nothing; 1 - only non-periodical; 2 - all

# INTERNAL VARIABLES
cycle_period = 0.0
status_active = 1
init_error = 'No error'
act_ppg_status = -1
act_pulses = set()
timing_table = []
output_table = []
newstatus_queue = queue.Queue(maxsize=20)
# This queue will be used to transfer new status info from Polling Thread to PPG-Status TCP-Server
# There is the danger that this queue will have msg-pile-up. This can not be avoided in general, so
# max size is limited to 20, and a warning is included in the log once the limit is reached. In this
# case one has to (i) increase the PPG_STATUS_READOUT_INTERVAL, (ii) improve the network connection,
# or (iii) reduce the status change rate in the FPGA.
# Increasing the QueueSize can only help for short time periods.

testid = 0  # TODO: test variable, will be removed later


def execute_command(data: str) -> str:
    """
    Send a command to the MUPPET FPGA and returns the answer. This is the only place
    in code where the FPGA access happens to be sure that these are atomic actions.
    The command and possible parameters are separated via '_' in data.

    Parameters
    ----------
    data : String
        Command String. Only capital letters used, parameters saparated with '_'.

    Returns
    -------
    String
        Answer string from the FPGA. In almost all cases this answer is of the form
        'CMD_(OK|ERROR)'.
    """
    global init_error
    global testid
    global act_ppg_status
    splidata = data.split('_', 1)
    cmd = splidata[0]
    if init_error == 'No error' or cmd == 'INIT':  # if Init_error active only a new INIT is allowed
        try:
            global cycle_period  # the cycle_period will be always the longest channel
            global status_active
            global act_pulses
            global timing_table
            global output_table
            if len(splidata) > 1:
                para = splidata[1]
            if cmd == 'INIT':  # more a reset: enable all channels, set them to non-inverted, and reset the cycle_period; will be done during startup of MUPPET, so does not need to be send manually to device
                initMUPPET()
                if init_error == 'No error':
                    return 'INIT_OK'
                else:
                    return init_error
            elif cmd == 'TESTCMD':
                time.sleep(5)
                testid = testid + 1
                return 'TEST_FINISHED_' + str(testid) + '_' + para
            elif cmd == 'READDATA':
                time.sleep(5)
                testid = testid + 1
                return 'DATA_' + str(testid) + '_' + para
            elif cmd == 'PPG-START':  # start PPG; parameter determines mode (0 - free running, 1 - ext trigger, 2 - ext trigger single shot, other - last setting)
                if int(para) == 0:
                    ms.seq_free_running(1)
                    ms.seq_ext_trig_en(0)
                elif int(para) == 1:
                    ms.seq_free_running(0)
                    ms.seq_ext_trig_en(1)
                elif int(para) == 2:
                    ms.seq_free_running(0)
                    ms.seq_ext_trig_en(0)
                    ms.seq_arm_single_shot()
                ms.sequencer_enable()  # enable MUPPET pulser
                return 'PPG-START_OK'
            elif cmd == 'PPG-ABORT':  # abort PPG; this command abort the pattern resets the device, and resets the trigger flags
                ms.seq_free_running(0)
                ms.seq_ext_trig_en(0)
                ms.sequencer_reset()
                return 'PPG-ABORT_OK'
            elif cmd == 'PPG-PULSE':  # configure one PPG pulse with times, enable it and set the cycle_period if necessary
                pulsepara = para.split('_', 2)
                ms.pulse(ch=int(pulsepara[0]), start=pulsepara[1], width=pulsepara[2])
                ms.enable_ch(int(pulsepara[0]))
                act_pulses.add((int(pulsepara[0]), pulsepara[1], pulsepara[2]))
                if (float(pulsepara[1]) + float(pulsepara[2])) > cycle_period:
                    cycle_period = float(pulsepara[1]) + float(pulsepara[2])
                    ms.set_cycle_period(str(cycle_period))
                    printMsg('Set new cycle period: ' + str(cycle_period), 1)
                ms.configure_ppg_channels()
                ms.seq_update_config()
                return 'PPG-PULSE_OK'
            elif cmd == 'PPG-TABLE':  # PPG configuration via sending of a timing and output table, a ClearPulses Cmd is executed first!
                ms.clear_pulses()
                cycle_period = 0
                ms.set_cycle_period('4n')
                ms.seq_update_config()
                act_pulses = set()
                pulsepara = para.split('_', 2)  # 1st element: timing list, 2nd: output list
                timing_table = list(map(lambda v: float(v), pulsepara[0].split(',')))
                output_table = pulsepara[1].split(',')
                pulses = ppg_tables_2_pulses(timing_table, output_table)
                new_cycle = False
                for pulse in pulses:
                    ms.pulse(ch=int(pulse[0]), start=pulse[1], width=pulse[2])
                    ms.enable_ch(int(pulse[0]))
                    act_pulses.add((int(pulse[0]), pulse[1], pulse[2]))
                    if (float(pulse[1]) + float(pulse[2])) > cycle_period:
                        cycle_period = float(pulse[1]) + float(pulse[2])
                        new_cycle = True
                if new_cycle:
                    ms.set_cycle_period(str(cycle_period))
                    printMsg('Set new cycle period: ' + str(cycle_period), 1)
                ms.configure_ppg_channels()
                ms.seq_update_config()
                return 'PPG-TABLE_OK'
            elif cmd == 'PPG-CLEARPULSES':  # clear configured pulses of all channels
                ms.clear_pulses()
                cycle_period = 0
                ms.set_cycle_period('4n')
                ms.seq_update_config()
                act_pulses = set()
                timing_table = []
                output_table = []
                return 'PPG-CLEARPULSES_OK'
            elif cmd == 'PPG-GETPULSES':
                result_string = ''
                for act_pulse in act_pulses:
                    result_string += '_' + ','.join(str(e) for e in act_pulse)
                return 'PPG-GETPULSES' + result_string
            elif cmd == 'PPG-GETTABLES':
                if len(timing_table) > 0:
                    return 'PPG-GETTABLES_' + ','.join(str(e) for e in timing_table) + '_' + ','.join(output_table)
                else:
                    return 'PPG-GETTABLES_No Tables!'
            elif cmd == 'PPG-STATUS':  # Returns the current status of the device - the FPGA is only asked for a new value if the periodic readout is switched off!
                if status_active == 1:
                    return 'PPG-STATUS_' + str(act_ppg_status)
                else:
                    return 'PPG-STATUS_' + str(ms.get_seq_running())
            elif cmd == 'PPG-POLL-STATUS':  # Automatic Polling of the Status from PollingThread
                newstat = ms.get_seq_running()
                if newstat != act_ppg_status:
                    act_ppg_status = newstat
                    try:
                        newstatus_queue.put(str(act_ppg_status), block=False)
                    except queue.Full:
                        printMsg('WARNING: Status-Queue is full, resetting...', 1)
                        while not newstatus_queue.empty():  # Remove all remaining elements in queue
                            try:
                                newstatus_queue.get(block=False)
                            except queue.Empty:
                                continue
                return str(act_ppg_status)
            elif cmd == 'PPG-STATUS-ACTIVE':  # Possibility to switch off the periodic status polling to the FPGA
                if int(para) == 0:
                    status_active = 0
                else:
                    status_active = 1
                return 'PPG-STATUS-ACTIVE_OK'
            else:
                printMsg('Command unknown!', 1)
                return cmd + '_ERROR: Cmd unknown!'
        except Exception as err:
            printMsg('Error occured: ' + str(err), 1)
            return cmd + 'ERROR: ' + str(err)
    else:
        printMsg('Init Error: ' + init_error, 1)
        return cmd + 'ERROR_(INIT): ' + init_error


def initMUPPET():
    """
    Initializes and reboots the MUPPET board. Resets the cycle_period.

    Returns
    -------
    None.

    """
    global cycle_period
    global init_error
    try:
        ms.init_sequencer()
        ms.reboot_fpga()
        ms.clear_registers()
        ms.reset_channels()
        cycle_period = 0
        ms.set_cycle_period("4n")
        ms.seq_update_config()
        init_error = 'No error'
    except Exception as err:
        init_error = str(err)


def printMsg(message: str, debug_lvl: int):
    """
    Prints a debug message with time stamp to the bash

    Parameters
    ----------
    message : String
        Message to be printed to the console.
    debug_lvl : Integer
        Debug level for this message: 1 - Event-Driven message, 2 - Periodic Message.

    Returns
    -------
    None.

    """
    if DEBUG_LVL >= debug_lvl:
        tstamp = time.strftime("%H:%M:%S - %d %b %Y", time.localtime())
        print('(' + tstamp + ') ' + message, flush=True)


def is_socket_closed(sock: socket.socket) -> bool:
    """
    Checks if the TCP/IP socket is closed or not.

    Parameters
    ----------
    sock : socket.socket
        TCP socket to be checked.

    Returns
    -------
    Boolean
        True if the socket is closed, if not false.

    """
    try:
        # this will try to read bytes without blocking and also without removing them from buffer (peek only)
        data = sock.recv(16, socket.MSG_DONTWAIT | socket.MSG_PEEK)
        if len(data) == 0:
            return True
    except BlockingIOError:
        return False  # socket is open and reading from it would block
    except ConnectionResetError:
        return True  # socket was closed for some other reason
    except Exception:
        return False
    return False


def ppg_cmdparser_thread(stop_event: threading.Event()):
    """
    TCP/IP server to get access to this script. The server opens a TCP port,
    waits for an external connection, and then for external ASCII commands
    terminated by the termination char, and once a command arrives, forward this
    command to the execute_command-function and therefore to the FPGA. Returns
    the answer.

    Parameters
    ----------
    stop_event : threading.Event()
        Event used to stop this thread.
    zeromq_int_pub_socket : zmq.Context().socket(zmq.PUB)  # zmq entfernen
        0MQ Publish socket to forward to execte-command-function.

    Returns
    -------
    None.

    """
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PPG_CMD_PORT))
    s.listen(1)
    while 1:
        try:
            if stop_event.is_set():
                break
            printMsg('Waiting for PPG_CMD connection (Ctrl+C to abort)', 1)
            conn, addr = s.accept()
            printMsg('Connected to PPG_CMD client IP:' + str(addr), 1)
            data = ''
            while conn:
                try:
                    if stop_event.is_set():
                        break
                    chunk = conn.recv(1024)
                    if not chunk:
                        break
                    data += chunk.decode('utf-8')
                    while TCP_TERM_CHAR in data:
                        line, data = data.split(TCP_TERM_CHAR, 1)
                        printMsg('New cmd: ' + line, 1)
                        answer = execute_command(line)
                        conn.sendall(bytes(answer + TCP_TERM_CHAR, 'utf-8'))
                except OSError:  # One can get a "No route to host" error if host crashed during connection
                    printMsg('PPG_CMD OSError', 1)
                    break
                except ConnectionResetError:  # One can get this error if host was restarted during connection
                    printMsg('PPG_CMD Connection reset by peer', 1)
                    break
        finally:
            conn.close()  # still small error in if Ctrl+C is pressed befor a client was connected
            printMsg('PPG_CMD Connection Closed', 1)


def zeromq_server_thread(stop_event: threading.Event()):
    """
    0MQ REQ-RPLY Server for the communication with either jupyter or (with small changes
    to the source code) with external 0MQ clients.

    Parameters
    ----------
    stop_event : threading.Event()
        Event used to stop this thread.

    Returns
    -------
    None.

    """
    def send_zeromq_reply(message: str, zeromq_socket: zmq.Context().socket(zmq.REP)):
        """
        Reply-Message to requests from outside 0MQ-connections. This can be either
        the JupyterNotebook, another python script, or a 0MQ command.
        Right now the actual time is added to the return string.

        Parameters
        ----------
        message : String
            Raw message to be sent as the reply.
        zeromq_socket : zmq.Context().socket(zmq.REP)
            0MQ Reply socket to answer to requests from outside.

        Returns
        -------
        None.

        """
        t = time.localtime()
        tstamp = time.strftime("%H:%M:%S - %d %b %Y", t)
        zeromq_socket.send_string(message + ' (' + tstamp + ')')

    context = zmq.Context()
    izrs = context.socket(zmq.REP)
    izrs.bind('ipc:///tmp/MUPPET_ZeroMQ_IPC')  # Default Socket, only (RasPi-) internal communication
    # izrs.bind('tcp://*:5555') # TCP Socket for external 0MQ communication on all available interfaces on port 5555
    izrs.setsockopt(zmq.RCVTIMEO, 100)
    while 1:
        try:
            if stop_event.is_set():
                break
            zmq_request_msg = izrs.recv_string()
            printMsg('From 0MQ: ' + zmq_request_msg, 1)
            cmd_return = execute_command(zmq_request_msg)
            printMsg('From 0MQ: ' + str(cmd_return), 1)
            send_zeromq_reply(cmd_return, izrs)
        except zmq.error.Again:  # Error thrown if 0MQ-Server has not yet opened socket or recv goes in TimeOut
            pass


def epics_thread(stop_event: threading.Event()):
    """
    Another TCP/IP server to get access to the FPGA. This server should be used
    to connect an EPICS IOC to.

    Parameters
    ----------
    stop_event : threading.Event()
        Event used to stop this thread.

    Returns
    -------
    None.

    """
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, EPICS_PORT))
    s.listen(1)
    while 1:
        try:
            if stop_event.is_set():
                break
            printMsg('Waiting for EPICS connection (Ctrl+C to abort)', 1)
            conn, addr = s.accept()
            printMsg('Connected to EPICS client IP:' + str(addr), 1)
            data = ''
            while conn:
                try:
                    if stop_event.is_set():
                        break
                    chunk = conn.recv(1024)
                    if not chunk:
                        break
                    data += chunk.decode('utf-8')
                    while TCP_TERM_CHAR in data:
                        line, data = data.split(TCP_TERM_CHAR, 1)
                        printMsg('New cmd (EPICS): ' + line, 1)
                        answer = execute_command(line)
                        conn.sendall(bytes(answer + TCP_TERM_CHAR, 'utf-8'))
                except OSError:  # One can get a "No route to host" error if host crashed during connection
                    printMsg('EPICS OSError', 1)
                    break
                except ConnectionResetError:  # One can get this error if host was restarted during connection
                    printMsg('EPICS Connection reset by peer', 1)
                    break
                except Exception as err:
                    printMsg(f'EPICS: Unexpected {err=}, {type(err)=}', 1)
                    raise
        finally:
            conn.close()  # still small error in if Ctrl+C is pressed befor a client was connected
            printMsg('EPICS Connection Closed', 1)


def ppg_status_thread(stop_event: threading.Event()):
    """
    TCP/IP server to publish the ppg status information as soon as it is changing.

    Parameters
    ----------
    stop_event : threading.Event()
        Event used to stop this thread.

    Returns
    -------
    None.

    """
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PPG_STATUS_PORT))
    s.listen(1)
    while 1:
        try:
            if stop_event.is_set():
                break
            printMsg('Waiting for PPG_STATUS connection (Ctrl+C to abort)', 1)
            conn, addr = s.accept()
            printMsg('Connected to PPG_STATUS client IP:' + str(addr), 1)
            while conn:
                try:
                    if is_socket_closed(conn):
                        break
                    if stop_event.is_set():
                        break
                    if status_active > 0:
                        try:
                            stat = newstatus_queue.get(block=True, timeout=0.1)
                            conn.sendall(stat.encode() + bytes(TCP_TERM_CHAR, 'utf-8'))
                        except queue.Empty:
                            pass
                except OSError:  # One can get a "No route to host" error if host crashed during connection
                    printMsg('PPG_STATUS OSError', 1)
                    break
                except ConnectionResetError:  # One can get this error if host was restarted during connection
                    printMsg('PPG_STATUS Connection reset by peer', 1)
                    break
        finally:
            conn.close()  # still small error in if Ctrl+C is pressed befor a client was connected
            printMsg('PPG_STATUS Connection Closed', 1)


def ppg_polling_thread(stop_event: threading.Event()):
    """
    The purpose of this thread is only to create status-polling commands for
    the PPG device.

    Parameters
    ----------
    stop_event : threading.Event()
        Event used to stop this thread.

    Returns
    -------
    None.

    """
    while 1:
        try:
            if stop_event.is_set():
                break
            if status_active > 0:
                execute_command('PPG-POLL-STATUS')
                time.sleep(PPG_STATUS_READOUT_INTERVAL)
        except Exception:
            pass
    printMsg('PPG_POLLING Thread Closed', 1)


def ppg_tables_2_pulses(timing_table: List[float], output_table: List[str]) -> set:
    """
    Converts the PPG configuration given in the form of a timing and an
    output list into a list of single pulses.

    Parameters
    ----------
    timing_table : List[float]
        A list where each entry is assigned a timing value. To combine this
        with the output list, the first entry is given the "name" "A", the
        second one "B" and so on.
    output_table : List[str]
        A list where each entry is connected to an ouput of the PPG. The
        entries of this list contain pattern of letters. With the help of the
        timing_table, a complex output pattern can be created.

    Returns
    -------
    set
        A set of single pulses. Each pulse consists of (i) Channel No.
        starting at 0, (ii) Start time, and (iii) pulse width.

    """
    resulting_pulses = set()
    chan_no = 0
    for out in output_table:
        act_pulse_pos = 0.0
        act_pulse_start = 0.0
        act_pulse_width = 0.0
        for letter_no in range(len(out)):
            if out[letter_no].isupper() and act_pulse_width == 0.0:  # rising edge
                act_pulse_start = act_pulse_pos
            act_letter_time = timing_table[ord(out[letter_no].lower()) - 97]
            if out[letter_no].isupper():
                act_pulse_width += act_letter_time
            if (not out[letter_no].isupper() or letter_no + 1 == len(out)) and act_pulse_width != 0.0:  # falling edge (or last entry)
                resulting_pulses.add((chan_no, act_pulse_start, act_pulse_width))
                act_pulse_width = 0.0
            act_pulse_pos += act_letter_time
        chan_no += 1
    return resulting_pulses


if __name__ == '__main__':
    """
    Creates and starts all threads and waits until they are closed.
    """
    printMsg('INIT MUPPET', 1)
    printMsg('***********', 1)
    printMsg('EPICS Active: ' + str(EPICS_ACTIVE), 1)
    printMsg('PPG Status Readout Interval: ' + str(PPG_STATUS_READOUT_INTERVAL), 1)
    printMsg('Debug Level: ' + str(DEBUG_LVL), 1)
    initMUPPET()  # Init MUPPET
    # Start the threads
    stop_event = threading.Event()
    ppgcmdthread = threading.Thread(target=ppg_cmdparser_thread, args=(stop_event,))
    ppgstatthread = threading.Thread(target=ppg_status_thread, args=(stop_event,))
    zmqthread = threading.Thread(target=zeromq_server_thread, args=(stop_event,))
    ppgpollthread = threading.Thread(target=ppg_polling_thread, args=(stop_event,))
    if EPICS_ACTIVE:
        epicsthread = threading.Thread(target=epics_thread, args=(stop_event,))
    ppgcmdthread.start()
    ppgstatthread.start()
    zmqthread.start()
    ppgpollthread.start()
    if EPICS_ACTIVE:
        epicsthread.start()
    try:
        ppgcmdthread.join()
        ppgstatthread.join()
        zmqthread.join()
        ppgpollthread.join()
        if EPICS_ACTIVE:
            epicsthread.join()
    except KeyboardInterrupt:
        stop_event.set()
        printMsg('\nUser Stop', 1)
    execute_command('INIT')  # resets the device at Shutdown to set it in well-known state; if needed, this line can be deleted

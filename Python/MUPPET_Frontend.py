#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is a small program to get status information from the MUPPET_Backend
script. The idea is that the definitions in this script can be executed
by a jupyter notebook.

Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Dennis Neidherr, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: D.Neidherr@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""

import zmq
from IPython.display import HTML, display
import numpy as np
from tabulate import tabulate

context = zmq.Context()
zs = context.socket(zmq.REQ)
zs.connect('ipc:///tmp/MUPPET_ZeroMQ_IPC')  # Default Socket, only (RasPi-) internal communication
# zs.connect('tcp://127.0.0.1:5555')  # TCP Socket for external ZeroMQ communication on localhost on port 5555


def ppg_status():
    zs.send(b'PPG-STATUS')
    message = zs.recv_string()
    print("Received reply %s" % (message))


def ppg_start(mode):
    zs.send(bytes('PPG-START_' + str(mode), 'utf-8'))
    message = zs.recv_string()
    print("Received reply %s" % (message))


def ppg_abort():
    zs.send(bytes('PPG-ABORT', 'utf-8'))
    message = zs.recv_string()
    print("Received reply %s" % (message))


def ppg_pulse(channel, start, width):
    zs.send(bytes('PPG-PULSE_' + str(channel) + '_' + str(f'{start:.9E}') + '_' + str(f'{width:.9E}'), 'utf-8'))
    message = zs.recv_string()
    print("Received reply %s" % (message))


def ppg_table(timing_t, output_t):
    zs.send(bytes('PPG-TABLE_' + ','.join(str(e) for e in timing_t) + '_' + ','.join(output_t), 'utf-8'))
    message = zs.recv_string()
    print("Received reply %s" % (message))


def ppg_clearpulses():
    zs.send(bytes('PPG-CLEARPULSES', 'utf-8'))
    message = zs.recv_string()
    print("Received reply %s" % (message))


def ppg_getpulses():
    zs.send(bytes('PPG-GETPULSES', 'utf-8'))
    message = zs.recv_string()
    result = []
    para = message.split(' ', 1)
    data = para[0].split('_')
    del data[0]
    for pulse in data:
        entries = pulse.split(',')
        start = float(entries[1]) * 1E9
        width = float(entries[2]) * 1E9
        result.append([int(entries[0]), start, width])
    nparray = np.array(result)
    sorted_nparray = nparray[np.lexsort((nparray[:, 1], nparray[:, 0]))]
    display(HTML(tabulate(sorted_nparray, headers=['channel no.', 'start (ns)', 'width (ns)'], tablefmt="html")))
    print("Received reply at %s" % (para[1]))


def ppg_gettables():
    zs.send(bytes('PPG-GETTABLES', 'utf-8'))
    message = zs.recv_string()
    para = message.split(' ', 1)
    data = para[0].split('_')
    del data[0]
    timing = data[0].split(',')
    timing_result = []
    for i in range(len(timing)):
        timing_result.append([chr(97 + i), float(timing[i]) * 1E9])
    output = data[1].split(',')
    output_result = []
    for i in range(len(output)):
        output_result.append([i, output[i]])
    timing_table = tabulate(timing_result, headers=['Letter', 'Time (ns)'], tablefmt="html")
    output_table = tabulate(output_result, headers=['Output No.', 'Pattern'], tablefmt="html")
    html_content = f"""
    <table style="width:50%">
      <tr>
        <td>{timing_table}</td>
        <td>{output_table}</td>
      </tr>
    </table>
    """
    display(HTML(html_content))
    print("Received reply at %s" % (para[1]))


def ppg_status_active(on):
    zs.send(bytes('PPG-STATUS-ACTIVE_' + str(on), 'utf-8'))
    message = zs.recv_string()
    print("Received reply %s" % (message))


def backend_log():
    f = open('/tmp/MUPPET_Backend.log', 'r')
    print(f.read())
    f.close()


def jupyter_log():
    f = open('/tmp/MUPPET_Jupyter.log', 'r')
    print(f.read())
    f.close()

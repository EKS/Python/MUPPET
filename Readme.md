MUPPET README
===================

![MUPPET board](Documentation/Figures/Intro_MUPPETBoard.png)*Picture of the MUPPET board*

MUPPET is a multi-purpose FPGA based PCB board for experimental control and data acquisition. The heart of the board is an ARTIX-7 FPGA
from Xilinx with an internal clock speed of up to 600 MHz. In the standard configuration of the board eight LEMO inputs and eight LEMO outputs (TTL or NIM) can be used to connect
the board to other experimental equipment. On top of that an extension board allows up to 40 more single-ended signals. The VHDL code running on the FPGA is open source
and can therefore be adopted by the user in order to fulfill a wide variety of application specific needs (https://git.gsi.de/fcmf/muppet1_sequencer_test). Communication with the board 
from another computer is possible via the TCP connection of a Raspberry Pi mounted directly onto the MUPPET board or directly via the Ethernet connector of the board itself. 

The software within this package can be installed on the RasPi and creates a TCP/IP server for an easy access to the device via Ethernet. It can be used without modifications to run
the FPGA as a pulse-pattern-generator (PPG) to control an experimental cycle and as a time-to-digital converter (TDC) for data acquisition in parallel or it can serve
as a starting point for user-specific developments.

<div align="center">
    ![Different software layers](Documentation/Figures/Intro_SoftwareLayers.png)
</div>

<div align="center">
    <i>Schematic View of the Different Software Layers</i>
</div>


The figure above shows the data flow with some of the included interfaces. The VHDL code runs independently on the FPGA. To communicate with the standard version of this code
a low-level Python script is already prepared (https://git.gsi.de/fcmf/pyfcmf) and as a sub-module  part of this repository. This code directly accesses the registers and memories of the FPGA. You can also find a
Jupyter Notebook in this repository for manual control.

For automatic control of this script and therefore the FPGA, the software in this repository can be used. To make the installation easy, all modules are packed in podman
containers. But if wanted the code can also run directly on the RasPi with small modifications. For different interfaces and purposes there are several different containers
prepared:

- The MUPPET container is the base container and should always run. It includes a TCP/IP server and transfers commands directly to the FPGA. It also supports
commands via Jupyter Notebooks, via the EPICS container, or via zeroMQ (has to be activated in the code).
- (optional) The EPICS container runs an IOC with StreamDevice and is configured to talk with the MUPPET container. With this container running in addition the integration
into an already existing EPICS system should be straight forward.
- (optional) In the future there will be also an MM-Sequencer container which runs a Python script with a special sequencer, needed by many nuclear-physics trap experiments.

## 1. Connections

In the standard configuration (so with the standard VHDL code) the outputs are the in the upper row counting from 1 to 8 whereas the inputs are on the lower row. The PPG needs input No. 8 (so the one on the far right)
as an external trigger. Input No. 6 can be used for an external 10 MHz clock signal.


## 2. Installation

This section describes the standard installation procedure for the TCP server. The *MUPPET_Backend.py* script can also be started directly on the RasPi but for this one has to install more packages to the Pi (see the MUPPET dockerfile). 

* **2.1:** Go to the Raspberry PI homepage and download the Imager (https://www.raspberrypi.com/software/). Install a Debian Bullseye 64-bit version (this software is 
not yet tested under Debian Bookworm and a first test showed problems with the EPICS container). Activate SSH if needed. If you need to set a static IP address you can configure 
this once the image is built onto the SD card. For this go to the image, open the file *cmdline.txt* and enter just after *rootwait*: *ip=x.x.x.x*.
* **2.2:** After the first login, the SPI interface has to be activated. 
	* Via VNC you can connect to the Pi. Then, go to *Preferences* -> *Raspberry Pi Configuration* and activate it:
        <div align="center">
        ![Activate the SPI Interface](Documentation/Figures/Installation_ActivateSPI.png){width=300}
	    
	    *Activate the SPI Interface in the RasPi Settings*
	    </div>
    * To activate SPI via SSH, just enter
	    ```
		sudo nano /boot/config.txt
		```
		activate the line *dtparam=spi=on* and restart the RasPi via *sudo reboot*.
* **2.3:** Create a new SPI mapping. For this, open a terminal and enter:
    ```
    sudo nano /etc/rc.local
    ```
    and add before the *exit 0* line the following:
    ```
    dtoverlay spi1-1cs cs0_pin=16
    ```
    Note that a similar command (*dtoverlay=spi1-1cs,cs0_pin=16*) should also work if appended to */boot/config.txt* but this did not work out for me. One can also
    activate this mapping manual by entering *sudo dtoverlay spi1-1cs cs0_pin=16* in a terminal but the previous way survives a restart of the ResPi.
* **2.4:** Enter the following commands in a terminal to download and extract the needed files:
    ```
    git clone --recursive --branch main https://git.gsi.de/EKS/Python/MUPPET.git ~/MUPPET
    ```
* **2.5:** Once everything is downloaded the podman container has to be build. For this, one first has to install podman on the RasPi. So open a terminal:
    ```
	sudo apt update
	sudo apt install podman
	```
* **2.6:** In the newly created MUPPET folder you can find a script called *Build.sh*. This activates
the build process, starts the container for the first time and activates the autostart. 
If you need more containers (see above) you can edit the build script. But note, that especially the EPICS image needs very long to be built on an RasPi (~160 minutes on a RasPi3B-1GB;
in the future I will try to upload the EPICS container image; cross compilation on a linux desktop PC is unfortunately not that simple):
    ```
	~/MUPPET/Build.sh
	```

* **2.7:** Restart the RasPi:
    ```
	sudo shutdown -r now
	```

## 3. Start and control the application

Once the build process is finished, the container is automatically started (and restarted if the *shutdown -r now* command was executed) and can be controlled with the scripts in the */MUPPET*-folder. 
You can also open and modify them (for example comment out not needed commands or add other modules):
- **Start.sh:** This script starts the container if previously stopped. Sometimes one does not get error messages if the start process was not successful. For this one can use the command 
*podman container ls --all* to check if the container is still alive.
- **Stop.sh:** Stops the MUPPET service as well as the container (Note that it may be aborted, so there will be no error messages shown). Stopping only the container
is not enough since the service tries to restart the container.
- **OpenBash_XXX.sh:** These scripts open an additional (!) bash to a container. This means that you do not get direct error messages or log information as if you
would have run the script in a local terminal but you can have access to the internal file system of the container. For example in the MUPPET container you can find log information
from the main python scripts in */tmp/* for example for the backend script via the command:
    ```
	cat /tmp/MUPPET_Backend.log
	```
	You can leave the bash with *exit*.
- **ResetJupyterTemplates.sh:** The MUPPET container also contains a Jupyter server. Unfortunately I have not found an easy solution to make the default Jupyter Notebooks,
which should serve as examples, read-only and give the user write-access to the rest. So the workaround is, that the user has everywhere write-access (and this is persistent,
so you can also restart the RasPi) and add a script to reset the templates. Maybe one should inform the user to duplicate the template first before making any changes.

## 4. MUPPET TCP server

The main purpose of the central MUPPET container is to provide a TCP/IP server to communicate with the MUPPET board. For this, the *MUPPET_Backend.py* script is automatically
started at the end of the startup sequence of the container.
The server accepts normal ASCII commands which have to be terminated with a linefeed. Afterwards it returns an ASCII answer followed by a linefeed constant. The answer 
is in general the repeated command followed by an underscore '_' and an 'OK' or a possible error.
The following commands are included:

- **'INIT':** This command is normally not needed since the device initiates itself automatically during startup. If send, it resets all information in the FPGA.
- **'STARTPPG_N':** *N* is a natural number. This command starts a previously configured cycle. The parameter *N* decides in which mode the cycle will run. *0* means that 
the cycle will run in free-run mode. This means it will start immediately and continue automatically over and over again until an abort command is sent. *1* means that
in addition to *0* the first cycle waits for an external trigger. Once this trigger arrives it will again continue forever. *2* means that after a trigger only one cycle
is executed and the device will go to idle afterwards. Everything else will just start the pattern generation with the last trigger settings.
- **'ABORTPPG':** Stops the pattern generation immediately (within a running cycle if necessary).
- **'PULSE_N_DD_DD':** This command configures a single pulse. The first parameter *N* determines the output channel starting with 0. The second and the third parameters
are the starting time and the width of the pulse. The values are in SI units and can be also entered in scientific notation.
- **'CLEARPULSES':** This command clears the memory with the information about all pulses.
- **'STATUS':** This command returns the current status of the pattern generator. *0* means that the pattern is currently not running, *1* means it is running.
- **'STATUS-ACTIVE_N':** The status readout is somehow special. The FPGA cannot contact the TCP server by itself. But some sequencers need to be informed automatically
once the cycle is finished. In order not to poll via network every maybe 100 ms or even faster, the TCP server can be configured to poll the FPGA. And only if the
status has changed this new information is transferred via network. For this a second TCP socket is open. This socket does not wait for commands from outside but just
publishes new status information. With the *STATUS-ACTIVE* command one can switch on or off this function. *0* means off, *1* means on (default).

The script itself can be found in the */MUPPET/Python* subfolder and uses the pyfcmf submodule for the FPGA communication.
It starts three to four threads:
- **ppgcmdthread:** This thread opens a TCP port on which commands are expected and answers are returned. The possible commands can be seen in the very first function
of the *MUPPET_Backend.py* script and are described later in this section. The default port for this TCP connection is 62347.
- **ppgstatthread:** This thread opens a TCP port and publishes status changes. The default port for this connection is 62348.
- **zmqthread:** This is an internally needed thread which starts a zeroMQ server. It is also necessary for the transfer of commands from and to Jupyter notebooks. The user normally does not
realize that this thread is running. But one can reconfigure the source code a bit so that the zeroMQ server can also be seen from outside which adds an additional
interface.
- **epicsthread:** This thread starts another TCP server configured to be able to talk with the EPICS IOC in the EPICS container. Right now this thread is switched off via
a global variable by default.

There is a LabVIEW driver for the connection to this TCP server which can be found here: https://git.gsi.de/EE-LV/Drivers/MUPPET

## 5. Access via Jupyter Notebook
Via Jupyter the user has a very simple way to access the MUPPET board via a normal internet browser from within the local network. Just enter the IP address of the RasPi followed by *:8888*.
The standard password to login is just *MUPPET*. It can be changed by changing the *jupyter_server_config.json* file in the *\MUPPET\ConfigFiles* subfolder. It is not that difficult but will not
be described here. Just search the internet for it.

![First part of the MUPPET Jupyter notebook template](Documentation/Figures/JupyterNotebookTemplate.png)
*First Part of the MUPPET Jupyter Notebook Template*

Right now there is one Jupyter notebook installed in the container with the name *MUPPET_Template.ipynb*. Note that this file is **not** read-only. Any modifications are saved
even if the RasPi is restarted. A good way to start would be to start by duplicating the template file. The available commands are:

- **status():** Returns the current status  of the device.
- **startppg(mode):** Starts the generation of the pattern. The parameter *mode* is the same as in the TCP server above.
- **abbortppg():** Aborts the cycle, even if the pattern is currently running.
- **pulse(channel,start,width):** Configures a single pulse with *width* starting at *start*. Both numbers are in seconds.
- **clearpulses():** Deletes all pulses in memory.
- **status_active(on):** Switches on or off the status-polling functionality (see above).
- **backend_log():** Returns the log output of the *Backend* file running in the container. The default setting is that this file will be reset if the container restarts.
- **jupyter_log():** Returns the log output of the *Jupyter* file running in the container. The default setting is that this file will be reset if the container restarts.

## 6. Activate zeroMQ access
ZeroMQ is used internally for the communication between threads. The zeroMQ server is normally not visible to anyone outside the MUPPET container but this can be changed relatively
easy. The following steps have to be done in order to get access:

* **6.1:** Open the file *.../MUPPET/MUPPET_Backend.py*, go to the lines 201/202 (at the beginning of the *zeromq_server_thread* definition), and comment out the ipc binding and comment in
    the TCP binding.
* **6.2:** Open the file *.../MUPPET/MUPPET_Frontend.py* and do the same in lines 19/20.
* **6.3:** In the next step port 5555 has to be opened for the MUPPET container. For this open the *Build.sh* script and add *-p 5555:5555* just before *muppet_image* at the end.
* **6.4:** Stop the MUPPET application via *Stop.sh*.
* **6.5:** Run the build script again via *Build.sh*.

## 7. EPICS support
The EPICS support has to be activated. For this, one has to:
* Open the file *.../MUPPET/Python/MUPPET_Backend.py* and modify line 33.
* Activate the EPICS container. Open the file *.../MUPPET/Build.sh* and delete all *#* in the lines 6, 10, 15, 18, and 19. Run the script.
* Modify the files *.../MUPPET/Start.sh* and *.../MUPPET/Stop.sh* accordingly.

## 8. The MM sequencer



#!/bin/bash

# Change the directory for the Python Cache
export PYTHONPYCACHEPREFIX=/tmp

# Reset Jupyter LogFile and Start Jupyter Notebook Server
rm -f /tmp/MUPPET_Jupyter.log &&
touch /tmp/MUPPET_Jupyter.log &&
jupyter notebook --ip 0.0.0.0 --no-browser --allow-root -NotebookApp.allow_origin=* --NoteBookApp.allow_remote_access=1 &>> /tmp/MUPPET_Jupyter.log &

# Reset Backend Logfile and Start Python MUPPET_Backend script
rm -f /tmp/MUPPET_Backend.log &&
touch /tmp/MUPPET_Backend.log &&
python /home/MUPPET/Python/MUPPET_Backend.py >> /tmp/MUPPET_Backend.log &

# Wait for any process to exit
wait -n

# Exit with status of process that exited first
exit $?

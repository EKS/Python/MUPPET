#!../../bin/linux-x86_64/MUPPET

#- You may have to change MUPPET to something else
#- everywhere it appears in this file

< envPaths
epicsEnvSet ("STREAM_PROTOCOL_PATH", "/home/EPICS/builds/MUPPET/protocols")

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/MUPPET.dbd"
MUPPET_registerRecordDeviceDriver pdbbase

## Configure devices
drvAsynIPPortConfigure ("MUPPET_Device", "${IP_MUPPET=127.0.0.1}:62350")

## Load record instances
# Hmm, kein IF hier erlaubt????
# if [ -z ${DEV_PREFIX} ]
# then
# dbLoadRecords("db/MUPPET.db", "user=root, DEV_PREFIX=${DEV_PREFIX}")
# else
dbLoadRecords("db/MUPPET.db", "user=root, DEV_PREFIX=${DEV_PREFIX}:")
# fi

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=root"

